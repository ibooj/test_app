#### Подготовка к запуску
```bash
$ docker-compose up -d
$ pip install -r requerements.txt
$ python manage.py migrate
$ python manage.py createsuperuser
$ python manage.py content_gen  # генерация контента
$ python manage.py comment_gen  # генерация комментариев (долго, по желанию)

```
#### Запуск

```bash
$ celery -A app  worker -l info
```
Для поддержки тестирования push уведомлений на localhost, dev сервер запускаем командой:
```bash
$ python manage.py runsslserver localhost:8000
```
А также браузер chrome запускаем командой:
```bash
$ google-chrome --allow-insecure-localhost https://localhost:8000
```
Или для разрешения работы https на localhost высталвяем флаг Enabled в настройках хрома [chrome://flags/#allow-insecure-localhost](chrome://flags/#allow-insecure-localhost), так же можно ипользовать firefox этот бразуре более лоялен.
##### Для регистрации устройства на push уведомления, а также для взаимодействия с системой необходима авторизация
[https://localhost:8000/api-auth/login/](https://localhost:8000/api-auth/login/)
(для простого просмотра комментариев и комментируемых сущностей авторизация не нужна)
##### Регистрация устройства на push уведомления
Кликаем на пункт меню (Registration device on push notices) в шапке сайта,
подписка на комментарии к комментируемой сущности [https://localhost:8000/comments/{content_type_id}/{object_id}/subscribe_to/](https://localhost:8000/comments/{content_type_id}/{object_id}/subscribe_to/), запись добавляется простой отправкой запроса методом POST
##### Список доступных сущностей для комментирования.
[https://localhost:8000/](url)
##### Создание комментария к определенной сущности, с указанием сущности, к которой он относится.
[https://localhost:8000/comments/{content_type_id}/{object_id}/](https://localhost:8000/comments/{content_type_id}/{object_id}/)
##### Получение комментариев первого уровня для определенной сущности с пагинацией.
[https://localhost:8000/comments/{content_type_id}/{object_id}/?level=0](https://localhost:8000/comments/{content_type_id}/{object_id}/?level=0)
##### Получение всех дочерних комментариев для заданного комментария или сущности без ограничения по уровню вложенности. Корнем может являться пара идентифицирующая сущность или id комментария, являющегося корневым для данной ветки. Ответ должен быть таким, чтобы на клиенте можно было воссоздать иерархию комментариев.
для сущности [https://localhost:8000/comments/{content_type_id}/{object_id}/](https://localhost:8000/comments/{content_type_id}/{object_id}/)
для комментария [https://localhost:8000/comments/{content_type_id}/{object_id}/{parent_id}/](https://localhost:8000/comments/{content_type_id}/{object_id}/{parent_id}/)
##### Получение истории комментариев определенного пользователя.
[https://localhost:8000/comments/by-user/{author_id}/](https://localhost:8000/comments/by-user/{author_id}/)
##### Выгрузка в файл (например в xml-формате) всей истории комментариев по пользователю или сущности с возможностью указания интервала времени, в котором был создан комментарий пользователя (если не задан - выводить всё). Время ответа на первичный запрос не должно зависеть от объема данных в итоговой выгрузке.
с фильтрацией по дате [https://localhost:8000/comments/by-user/{author_id}/?created\_\_gte=2018-04-25&created\_\_lte=2018-04-27&format=xml](https://localhost:8000/comments/by-user/{author_id}/?created\_\_gte=2018-04-25&created\_\_lte=2018-04-27&format=xml)
к любому урл со списком сущностей, если добавлять параметр ?format=xml получаем ответ в xml, при этом пагинатор решил отключать, чтобы получить все данные, если данных много, то возможно процесс получения данных будет продолжительным из-за объема текстовых xml данных. 
Для фильтрации по дате так же можно задавать время через пробел, например - (?created\_\_gte=2018-04-25 20:20&created\_\_lte=...)
#### Дополнительные требования
##### Комментарии могут редактироваться и удаляться. Удаление возможно только, если у комментария нет дочерних комментариев. Реализовать хранение исторических данных с возможностью получения истории для определенного комментария: информация о том, кем и когда был изменен/удален комментарий, что изменилось в комментарии.
редактирование, удаление [https://localhost:8000/comments/edit/{id}/](https://localhost:8000/comments/edit/{id}/)
история изменений комментария [https://localhost:8000/admin/comments/comment/{id}/history/](https://localhost:8000/admin/comments/comment/{id}/history/)
##### Возможность подписки на события комментирования определенной сущности - при создании/редактировании/удалении комментария к этой сущности с сервера уходит PUSH-уведомление клиенту с информацией о созданном/отредактированным/удаленным комментарием в таком виде, чтобы клиент имел возможность динамически добавить/обновить/удалить его в интерфейсе.
[https://localhost:8000/comments/{content_type_id}/{object_id}/subscribe_to/](https://localhost:8000/comments/{content_type_id}/{object_id}/subscribe_to/)
##### Для пункта 5.е требований реализовать гибкий механизм с возможностью добавления различных форматов файлов
добавление файлов к комментарию [https://localhost:8000/comments/edit/{comment_id}/file/](https://localhost:8000/comments/edit/{comment_id}/file/)
удаление файла [https://localhost:8000/comments/edit/{comment_id}/file/{id}/](https://localhost:8000/comments/edit/{comment_id}/file/{id}/)
##### Документация к api
[https://localhost:8000/api-docs/](https://localhost:8000/api-docs/)
##### Для запуска тестов
```bash
$ python manage.py test
```
