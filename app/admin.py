from django.contrib import admin
from .models import NewsItem, ArticleItem


@admin.register(NewsItem)
class NewsItemAdmin(admin.ModelAdmin):
    list_display = ('title',)


@admin.register(ArticleItem)
class ArticleItemAdmin(admin.ModelAdmin):
    list_display = ('title',)
