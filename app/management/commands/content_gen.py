from django.core.management.base import BaseCommand
from ...models import NewsItem, ArticleItem
from faker import Faker


class Command(BaseCommand):

    def add_arguments(self, parser):
        parser.add_argument(
            '--clear',
            action='store_true',
            dest='clear',
            help='Delete all content',
        )

    def handle(self, *args, **options):
        if options['clear']:
            for o in NewsItem.objects.all():
                o.delete()
            for o in ArticleItem.objects.all():
                o.delete()
        else:
            fake = Faker()
            for i in range(5):
                NewsItem.objects.create(title=fake.text().split('.')[0])
                ArticleItem.objects.create(title=fake.text().split('.')[0])
