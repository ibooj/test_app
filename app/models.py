from django.contrib.contenttypes.models import ContentType
from django.db import models


class AbstractContentItem(models.Model):
    title = models.CharField(max_length=300)
    created = models.DateTimeField(auto_now_add=True)

    def get_content_type(self):
        return ContentType.objects.get_for_model(self)

    class Meta:
        abstract = True
        ordering = ['-created']


class NewsItem(AbstractContentItem):
    pass


class ArticleItem(AbstractContentItem):
    pass
