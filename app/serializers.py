from rest_framework import serializers
from rest_framework.reverse import reverse_lazy

from .models import NewsItem, ArticleItem


class ContentSerializer(serializers.ModelSerializer):
    comments_url = serializers.SerializerMethodField()
    subscribe_to_url = serializers.SerializerMethodField()

    def get_comments_url(self, obj):
        return str(reverse_lazy('comments:comment-list', args=[obj.get_content_type().id, obj.id],
                                request=self.context['request']))

    def get_subscribe_to_url(self, obj):
        return str(reverse_lazy('comments:subscribe-to-detail', args=[obj.get_content_type().id, obj.id],
                                request=self.context['request']))


class NewsItemSerializer(ContentSerializer):
    class Meta:
        model = NewsItem
        fields = ('id', 'title', 'comments_url', 'subscribe_to_url')


class ArticleItemSerializer(ContentSerializer):
    class Meta:
        model = ArticleItem
        fields = ('id', 'title', 'comments_url', 'subscribe_to_url')
