from django.urls import reverse
from faker import Faker
from rest_framework import status
from rest_framework.test import APITestCase

from .models import NewsItem, ArticleItem


class ContentViewTest(APITestCase):
    def setUp(self):
        fake = Faker()
        for o in range(10):
            NewsItem.objects.create(title=fake.text().split('.')[0])
            ArticleItem.objects.create(title=fake.text().split('.')[0])

    def tearDown(self):
        for o in NewsItem.objects.all():
            o.delete()
        for o in ArticleItem.objects.all():
            o.delete()

    def test_root_api(self):
        response = self.client.get(reverse('root-api'))
        self.assertEqual(response.status_code, status.HTTP_200_OK, msg=response.json())

    def test_news_list(self):
        response = self.client.get(reverse('news-list'))
        self.assertEqual(response.status_code, status.HTTP_200_OK, msg=response.json())
        self.assertEqual(len(response.json().get('results')), 10, msg=response.json())

    def test_article_list(self):
        response = self.client.get(reverse('article-list'))
        self.assertEqual(response.status_code, status.HTTP_200_OK, msg=response.json())
        self.assertEqual(len(response.json().get('results')), 10, msg=response.json())
