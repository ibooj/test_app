from django.conf import settings
from django.conf.urls.static import static
from django.contrib import admin
from django.urls import path, include
from django.views.generic import TemplateView
from rest_framework.documentation import include_docs_urls
from .views import api_root, NewsList, ArticleList

urlpatterns = [
    path('admin/', admin.site.urls),
    path('api-auth/', include('rest_framework.urls', namespace='rest_framework')),

    path('', api_root, name='root-api'),
    path('news/', NewsList.as_view(), name='news-list'),
    path('articles/', ArticleList.as_view(), name='article-list'),
    path('comments/', include('comments.urls', namespace='comments')),
    path('api-docs/', include_docs_urls(title='API')),

    path('firebase-messaging-sw.js', TemplateView.as_view(template_name="comments/firebase-messaging-sw.js",
                                                          content_type='application/javascript')),
]

if settings.DEBUG:
    urlpatterns += static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)
    if 'debug_toolbar' in settings.INSTALLED_APPS:
        import debug_toolbar

        urlpatterns += [path('__debug__/', include(debug_toolbar.urls))]
