from rest_framework.decorators import api_view
from rest_framework.response import Response
from rest_framework.reverse import reverse
from rest_framework.generics import ListAPIView
from .serializers import NewsItemSerializer, ArticleItemSerializer
from .models import NewsItem, ArticleItem


@api_view(['GET'])
def api_root(request, format=None):
    return Response({
        'news': reverse('news-list', request=request, format=format),
        'articles': reverse('article-list', request=request, format=format)
    })


class NewsList(ListAPIView):
    queryset = NewsItem.objects.all()
    serializer_class = NewsItemSerializer


class ArticleList(ListAPIView):
    queryset = ArticleItem.objects.all()
    serializer_class = ArticleItemSerializer
