from django.contrib import admin
from .models import Comment, CommentFile, SubscribedUser
from django.utils.html import format_html
from reversion.admin import VersionAdmin


@admin.register(CommentFile)
class CommentFileAdmin(admin.ModelAdmin):
    list_display = ('file_source', 'id', 'content_link')

    def content_link(self, obj):
        if obj.comment:
            return format_html('<a href="{}">Comment #{}</a>'.format(obj.comment.get_admin_edit_url(), obj.comment.id))

    content_link.allow_tags = True


class CommentFileInline(admin.TabularInline):
    model = CommentFile


@admin.register(Comment)
class CommentAdmin(VersionAdmin):
    list_display = ('text', 'author', 'created', 'content_object')
    inlines = (CommentFileInline,)
    raw_id_fields = ('author', 'parent')


@admin.register(SubscribedUser)
class SubscribedUserAdmin(admin.ModelAdmin):
    list_display = ('content_object', 'author')
    raw_id_fields = ('author',)
