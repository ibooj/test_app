from django.core.management.base import BaseCommand
from ...models import Comment, CONTENT_TYPE_CHOICES
from django.contrib.contenttypes.models import ContentType
from django.contrib.auth.models import User
from faker import Faker


class Command(BaseCommand):

    def add_arguments(self, parser):
        parser.add_argument(
            '--clear',
            action='store_true',
            dest='clear',
            help='Delete all comments',
        )

    def handle(self, *args, **options):
        if options['clear']:
            for o in Comment.objects.all():
                o.delete()
        else:
            fake = Faker()
            user = User.objects.first()
            for ct in ContentType.objects.filter(CONTENT_TYPE_CHOICES):
                mc = ct.model_class()
                print(mc().__class__.__name__)
                for o in mc.objects.all()[:1]:
                    for a in range(10 ** 4):
                        parent = None
                        for b in range(100 if a < 2 else 2):
                            parent = Comment.objects.create(
                                parent=parent,
                                author=user,
                                object_id=o.id,
                                content_type=ct,
                                text=fake.text().split('.')[0],
                                skip_signal=True
                            )
