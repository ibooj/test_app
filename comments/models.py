import operator
import os
from functools import reduce, wraps

import reversion
from django.conf import settings
from django.contrib.contenttypes.fields import GenericForeignKey
from django.contrib.contenttypes.models import ContentType
from django.db import models
from django.db.models.signals import post_delete, post_save
from django.dispatch import receiver
from django.urls import reverse
from fcm_django.models import FCMDevice
from mptt.models import MPTTModel, TreeForeignKey

from .tasks import push_sender

CONTENT_TYPE_CHOICES = reduce(
    operator.or_,
    [models.Q(app_label=k, model=m) for k, v in
     getattr(settings, 'COMMENTED_CONTENT_TYPES', {'comments': ('comment',)}).items() for m in v]
)


def comment_directory_path(instance, filename):
    return 'comments/comment_{0}/{1}'.format(instance.comment.id, filename)


@reversion.register()
class Comment(MPTTModel):
    parent = TreeForeignKey('self', on_delete=models.CASCADE, null=True, blank=True, related_name='children')

    text = models.TextField(max_length=1000)
    author = models.ForeignKey(settings.AUTH_USER_MODEL, on_delete=models.CASCADE)
    content_type = models.ForeignKey(ContentType, on_delete=models.CASCADE, limit_choices_to=CONTENT_TYPE_CHOICES)
    object_id = models.PositiveIntegerField()
    content_object = GenericForeignKey('content_type', 'object_id')
    created = models.DateTimeField(auto_now_add=True)
    modified = models.DateTimeField(auto_now=True)

    @property
    def skip_signal(self):
        return self._skip_signal

    @skip_signal.setter
    def skip_signal(self, value):
        if value:
            self._skip_signal = True

    def get_admin_edit_url(self):
        return reverse('admin:%s_%s_change' % (self._meta.app_label, self._meta.model_name), args=[self.id])

    def get_absolute_url(self):
        return reverse('comments:comment-detail', args=[self.id])

    def get_comments_url(self):
        return reverse('comments:comment-list', args=[self.content_type.id, self.object_id])

    class MPTTMeta:
        order_insertion_by = ['created']


class CommentFile(models.Model):
    file_source = models.FileField(upload_to=comment_directory_path)
    comment = models.ForeignKey(Comment, related_name='files', on_delete=models.CASCADE)
    author = models.ForeignKey(settings.AUTH_USER_MODEL, on_delete=models.CASCADE)


@receiver(post_delete, sender=CommentFile)
def file_post_delete_handler(sender, instance, *args, **kwargs):
    storage, path = instance.file_source.storage, instance.file_source.path
    storage.delete(path)
    storage_dir = '/'.join(path.split('/')[:-1])
    if os.path.exists(storage_dir):
        if len(os.listdir(storage_dir)) == 0:
            os.rmdir(storage_dir)


class SubscribedUser(models.Model):
    author = models.ForeignKey(settings.AUTH_USER_MODEL, on_delete=models.CASCADE)
    content_type = models.ForeignKey(ContentType, on_delete=models.CASCADE, limit_choices_to=CONTENT_TYPE_CHOICES)
    object_id = models.PositiveIntegerField()
    content_object = GenericForeignKey('content_type', 'object_id')

    class Meta:
        unique_together = ('author', 'content_type', 'object_id')


def skip_signal():
    def _skip_signal(signal_func):
        @wraps(signal_func)
        def _decorator(sender, instance, **kwargs):
            if hasattr(instance, 'skip_signal'):
                return None
            return signal_func(sender, instance, **kwargs)

        return _decorator

    return _skip_signal


@receiver(post_save, sender=Comment)
@receiver(post_delete, sender=Comment)
@skip_signal()
def send_push_handler(sender, instance, created=None, *args, **kwargs):
    msg = {'title': '', 'body': 'Click to go to comment.'}
    if created is None:
        msg.update({'title': 'Comment deleted.', 'body': 'Click to go to commented object.',
                    'url': instance.get_comments_url()})
    else:
        if created:
            msg.update({'title': 'Comment created.'})
        else:
            msg.update({'title': 'Comment updated.'})
        msg.update({'url': instance.get_absolute_url()})

    if SubscribedUser.objects.filter(content_type=instance.content_type, object_id=instance.object_id).exists():
        push_sender.delay({'object_id': instance.object_id, 'content_type_id': instance.content_type.id}, msg)
