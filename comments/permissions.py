from rest_framework.permissions import (
    BasePermission, IsAuthenticated, IsAuthenticatedOrReadOnly as RestIsAuthenticatedOrReadOnly, SAFE_METHODS)


class IsPOSTRequest(BasePermission):
    def has_permission(self, request, view):
        return request.method == "POST"


class IsGETRequest(BasePermission):
    def has_permission(self, request, view):
        return request.method == "GET"


class IsDELETERequest(BasePermission):
    def has_permission(self, request, view):
        return request.method == "DELETE"


class IsPUTRequest(BasePermission):
    def has_permission(self, request, view):
        return request.method == "PUT"


class IsPATCHRequest(BasePermission):
    def has_permission(self, request, view):
        return request.method == "PATCH"


class IsOPTIONSRequest(BasePermission):
    def has_permission(self, request, view):
        return request.method == "OPTIONS"


class IsOwner(IsAuthenticated):
    def has_object_permission(self, request, view, obj):
        return obj.author == request.user


class IsAuthenticatedOrReadOnly(RestIsAuthenticatedOrReadOnly):
    def has_object_permission(self, request, view, obj):
        return request.method in SAFE_METHODS
