from django.contrib.auth.models import User
from rest_framework import serializers
from rest_framework.reverse import reverse_lazy

from .models import Comment, CommentFile, SubscribedUser


class AuthorSerializer(serializers.ModelSerializer):
    comments_url = serializers.HyperlinkedIdentityField('comments:user-comment-list', lookup_field='id',
                                                        lookup_url_kwarg='author_id')

    class Meta:
        model = User
        fields = ('id', 'username', 'comments_url')


class CommentFileSerializer(serializers.ModelSerializer):
    edit_url = serializers.SerializerMethodField()

    class Meta:
        model = CommentFile
        exclude = ('comment', 'author')

    def get_edit_url(self, obj):
        return str(reverse_lazy('comments:comment-file-detail', args=[obj.comment.id, obj.id],
                                request=self.context['request']))

    def validate_file_source(self, value):
        if not self.context['request'].user.is_authenticated or not Comment.objects.filter(
                pk=self.context['view'].kwargs['comment_id'], author=self.context['request'].user).exists():
            raise serializers.ValidationError("You don't have rights for this object")
        return value


class CommentListSerializer(serializers.ModelSerializer):
    answer_url = serializers.SerializerMethodField()
    add_files_url = serializers.SerializerMethodField()
    comment_entity_url = serializers.SerializerMethodField()
    edit_url = serializers.HyperlinkedIdentityField('comments:comment-detail')
    author = AuthorSerializer(read_only=True)
    files = CommentFileSerializer(many=True, read_only=True)

    class Meta:
        model = Comment
        exclude = ('lft', 'rght', 'tree_id', 'content_type', 'object_id')
        extra_kwargs = {
            'parent': {'read_only': True},
        }

    def get_answer_url(self, obj):
        return str(reverse_lazy('comments:answer-comment-list', args=[obj.content_type_id, obj.object_id, obj.id],
                                request=self.context['request']))

    def get_comment_entity_url(self, obj):
        return str(reverse_lazy('comments:comment-list', args=[obj.content_type_id, obj.object_id],
                                request=self.context['request']))

    def get_add_files_url(self, obj):
        return str(reverse_lazy('comments:comment-file-list', args=[obj.id], request=self.context['request']))


class SubscribedUserSerializer(serializers.ModelSerializer):
    class Meta:
        model = SubscribedUser
        exclude = ('author',)
        read_only_fields = ('content_type', 'object_id')
