const messaging = firebase.messaging();

messaging.onMessage(function (payload) {
    console.log("Message received. ", payload);
    let n = new Notification(payload.notification.title, payload.notification);
    n.onclick = function (e) {
        window.open(payload.notification.click_action, '_blank');
    }
});

messaging.onTokenRefresh(function () {
    messaging.getToken()
        .then(function (refreshedToken) {
            if (refreshedToken) {
                sendTokenToServer(refreshedToken);
            }
        })
        .catch(function (err) {
            console.log('Unable to retrieve refreshed token ', err);
        });
});


function sendTokenToServer(currentToken) {
    fetch(`/comments/devices/${currentToken}/`, {
        method: "GET",
        headers: {
            'Content-Type': 'application/json'
        },
        credentials: "include",
    }).then(function (response) {
            if (response.status == 404) {
                fetch('/comments/devices/', {
                    method: "POST",
                    headers: {
                        'Content-Type': 'application/json'
                    },
                    body: JSON.stringify({
                        'registration_id': currentToken,
                        'type': 'web',
                    }),
                    credentials: "include",
                }).then(function (response) {
                    console.log(response);
                });
            } else if (!response.ok) {
                messaging.deleteToken(currentToken)
                    .then(function () {
                        getToken();
                    })
                    .catch(function (err) {
                        console.log('Unable to delete token. ', err);
                    });
            }
            else {
                console.log("Token already sent to server so won't send it again unless it changes");
            }
        }
    );
}

function getToken() {
    messaging.getToken()
        .then(function (currentToken) {
            if (currentToken) {
                sendTokenToServer(currentToken);
            }
        })
        .catch(function (err) {
            console.log('An error occurred while retrieving token. ', err);
        });
}

function requestPermission() {
    messaging.requestPermission()
        .then(function () {
            console.log('Notification permission granted.');
            getToken();
        })
        .catch(function (err) {
            console.log('Unable to get permission to notify.', err);
        });
}

$(function () {
    $('a[href="#registration-device"]').click(function () {
        requestPermission();
        return false;
    });
    getToken()
});
