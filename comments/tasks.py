from celery_once import QueueOnce
from django.contrib.auth.models import User
from fcm_django.models import FCMDevice

from app.celery import app


@app.task(bind=True, base=QueueOnce, once={'graceful': True})
def push_sender(self, f, msg):
    filter_kwargs = {'subscribeduser__{}'.format(v): k for v, k in f.items()}
    queryset = FCMDevice.objects.filter(active=True, user__in=User.objects.filter(**filter_kwargs))
    if queryset:
        queryset.send_message(
            title=msg['title'],
            body=msg['body'],
            click_action=msg['url']
        )
    return True
