from django.test import override_settings
import os
import shutil
from django.conf import settings
from django.contrib.auth.models import User
from django.contrib.contenttypes.models import ContentType
from django.core.files.uploadedfile import SimpleUploadedFile
from django.urls import reverse
from django.utils import timezone
from faker import Faker
from rest_framework import status
from rest_framework.test import APITestCase

from .models import Comment, CONTENT_TYPE_CHOICES


class CommentListCreateTest(APITestCase):
    def setUp(self):
        self.fake = Faker()
        self.user = User.objects.create_user('test_user')
        self.content_type_object = ContentType.objects.filter(CONTENT_TYPE_CHOICES).first()
        self.content_object = self.content_type_object.model_class().objects.create(
            title=self.fake.text().split('.')[0])
        self.url_args = [self.content_type_object.id, self.content_object.id]
        self.comment_list = []
        self.today = timezone.now()
        self.yesterday = self.today - timezone.timedelta(days=1)
        for a in range(10):
            parent = None
            for b in range(3):
                parent = Comment.objects.create(
                    parent=parent,
                    author=self.user,
                    object_id=self.content_object.id,
                    content_type=self.content_type_object,
                    text=self.fake.text().split('.')[0],

                )
                parent.created = self.yesterday
                parent.save()
                self.comment_list.append(parent)

        for c in range(5):
            self.comment_list.append(Comment.objects.create(
                author=self.user,
                object_id=self.content_object.id,
                content_type=self.content_type_object,
                text=self.fake.text().split('.')[0],
            ))

    def tearDown(self):
        self.user.delete()

    def test_comment_list(self):
        response = self.client.get(reverse('comments:comment-list', args=self.url_args))
        self.assertEqual(response.status_code, status.HTTP_200_OK, msg=response.json())
        self.assertEqual(len(response.json().get('results')), 30)
        self.assertEqual(response.json().get('results')[0]['id'], response.json().get('results')[1]['parent'])
        self.assertEqual(response.json().get('results')[1]['id'], response.json().get('results')[2]['parent'])
        self.assertEqual(response.json().get('results')[3]['level'], 0)
        self.assertEqual(response.json().get('results')[3]['parent'], None)

    def test_comment_create(self):
        comment_create_link = reverse('comments:comment-list', args=self.url_args)
        data = {'text': self.fake.text().split('.')[0]}
        response = self.client.post(comment_create_link, data)
        self.assertEqual(response.status_code, status.HTTP_403_FORBIDDEN, msg=response.json())

        self.client.force_login(self.user)
        response = self.client.post(comment_create_link, data)
        self.assertEqual(response.status_code, status.HTTP_201_CREATED, msg=response.json())

    def test_answer_comment_create(self):
        answer_comment_create_link = reverse('comments:answer-comment-list',
                                             args=self.url_args + [self.comment_list[0].id])
        data = {'text': self.fake.text().split('.')[0]}
        response = self.client.post(answer_comment_create_link, data)
        self.assertEqual(response.status_code, status.HTTP_403_FORBIDDEN, msg=response.json())

        self.client.force_login(self.user)
        response = self.client.post(answer_comment_create_link, data)
        self.assertEqual(response.status_code, status.HTTP_201_CREATED, msg=response.json())

    def test_comment_list_with_filter(self):
        response = self.client.get(reverse('comments:comment-list', args=self.url_args), data={'level': 0})
        self.assertEqual(response.status_code, status.HTTP_200_OK, msg=response.json())
        self.assertEqual(len(response.json().get('results')), 15)
        self.assertEqual({i['level'] for i in response.json().get('results')}, {0})

        data_filter = {
            'created__gte': timezone.localtime(self.today).strftime('%Y-%m-%d %H:%M')
        }
        response = self.client.get(reverse('comments:comment-list', args=self.url_args), data=data_filter)
        self.assertEqual(response.status_code, status.HTTP_200_OK, msg=response.json())
        self.assertEqual(len(response.json().get('results')), 5)


class CommentDetailEditDeleteTest(APITestCase):
    def setUp(self):
        self.fake = Faker()
        self.user = User.objects.create_user('test_user')
        self.user_two = User.objects.create_user('test_user_two')
        self.content_type_object = ContentType.objects.filter(CONTENT_TYPE_CHOICES).first()
        self.content_object = self.content_type_object.model_class().objects.create(
            title=self.fake.text().split('.')[0])
        self.comment_list = []
        self.today = timezone.now()
        self.yesterday = self.today - timezone.timedelta(days=1)
        for a in range(10):
            parent = None
            for b in range(3):
                parent = Comment.objects.create(
                    parent=parent,
                    author=self.user,
                    object_id=self.content_object.id,
                    content_type=self.content_type_object,
                    text=self.fake.text().split('.')[0],

                )
                parent.created = self.yesterday
                parent.save()
                self.comment_list.append(parent)

        for c in range(5):
            self.comment_list.append(Comment.objects.create(
                author=self.user,
                object_id=self.content_object.id,
                content_type=self.content_type_object,
                text=self.fake.text().split('.')[0],
            ))

    def tearDown(self):
        self.user.delete()
        self.user_two.delete()

    def test_comment_detail(self):
        response = self.client.get(reverse('comments:comment-detail', args=[self.comment_list[0].id]))
        self.assertEqual(response.status_code, status.HTTP_200_OK, msg=response.json())

    def test_comment_update(self):
        data = {'text': self.fake.text().split('.')[0]}
        response = self.client.put(reverse('comments:comment-detail', args=[self.comment_list[0].id]), data)
        self.assertEqual(response.status_code, status.HTTP_403_FORBIDDEN, msg=response.json())

        self.client.force_login(self.user)
        response = self.client.put(reverse('comments:comment-detail', args=[self.comment_list[0].id]), data)
        self.assertEqual(response.status_code, status.HTTP_200_OK, msg=response.json())
        self.assertNotEqual(response.json()['text'], self.comment_list[0].text)

        self.client.force_login(self.user_two)
        response = self.client.put(reverse('comments:comment-detail', args=[self.comment_list[0].id]), data)
        self.assertEqual(response.status_code, status.HTTP_403_FORBIDDEN, msg=response.json())

    def test_comment_delete(self):
        response = self.client.delete(reverse('comments:comment-detail', args=[self.comment_list[0].id]))
        self.assertEqual(response.status_code, status.HTTP_403_FORBIDDEN, msg=response.json())

        self.client.force_login(self.user_two)
        response = self.client.delete(reverse('comments:comment-detail', args=[self.comment_list[0].id]))
        self.assertEqual(response.status_code, status.HTTP_403_FORBIDDEN, msg=response.json())

        self.client.force_login(self.user)
        response = self.client.delete(reverse('comments:comment-detail', args=[self.comment_list[0].id]))
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST, msg=response.json())

        self.client.force_login(self.user)
        response = self.client.delete(reverse('comments:comment-detail', args=[self.comment_list[-1].id]))
        self.assertEqual(response.status_code, status.HTTP_204_NO_CONTENT)


class CommentListByUserTest(APITestCase):
    def setUp(self):
        self.fake = Faker()
        self.user = User.objects.create_user('test_user')
        self.user_two = User.objects.create_user('test_user_two')
        self.content_type_object = ContentType.objects.filter(CONTENT_TYPE_CHOICES).first()
        self.content_object = self.content_type_object.model_class().objects.create(
            title=self.fake.text().split('.')[0])
        self.comment_list_user = []
        self.comment_list_user_two = []
        self.today = timezone.now()
        self.yesterday = self.today - timezone.timedelta(days=1)
        for a in range(5):
            c = Comment.objects.create(
                author=self.user,
                object_id=self.content_object.id,
                content_type=self.content_type_object,
                text=self.fake.text().split('.')[0],

            )
            c.created = self.yesterday
            c.save()
            self.comment_list_user.append(c)
        self.comment_list_user.append(Comment.objects.create(
            parent=self.comment_list_user[-1],
            author=self.user,
            object_id=self.content_object.id,
            content_type=self.content_type_object,
            text=self.fake.text().split('.')[0],

        ))

        for a in range(5):
            c = Comment.objects.create(
                author=self.user_two,
                object_id=self.content_object.id,
                content_type=self.content_type_object,
                text=self.fake.text().split('.')[0],

            )
            c.created = self.yesterday
            c.save()
            self.comment_list_user_two.append(c)
        self.comment_list_user_two.append(Comment.objects.create(
            parent=self.comment_list_user_two[-1],
            author=self.user_two,
            object_id=self.content_object.id,
            content_type=self.content_type_object,
            text=self.fake.text().split('.')[0],

        ))

    def tearDown(self):
        self.user.delete()
        self.user_two.delete()

    def test_comment_list_by_user(self):
        response = self.client.get(reverse('comments:user-comment-list', args=[self.user.id]))
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(len(response.json().get('results')), len(self.comment_list_user))

        response = self.client.get(reverse('comments:user-comment-list', args=[self.user_two.id]))
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(len(response.json().get('results')), len(self.comment_list_user_two))

        response = self.client.get(reverse('comments:user-comment-list', args=[self.user.id]), data={'level': 0})
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(len(response.json().get('results')), len(self.comment_list_user) - 1)

        data_filter = {
            'created__gte': timezone.localtime(self.today).strftime('%Y-%m-%d %H:%M')
        }
        response = self.client.get(reverse('comments:user-comment-list', args=[self.user.id]), data=data_filter)
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(len(response.json().get('results')), 1)


@override_settings(MEDIA_ROOT=os.path.join(settings.BASE_DIR, 'tests'))
class FileListCreateTest(APITestCase):
    def setUp(self):
        self.fake = Faker()
        self.user = User.objects.create_user('test_user')
        self.user_two = User.objects.create_user('test_user_two')
        self.content_type_object = ContentType.objects.filter(CONTENT_TYPE_CHOICES).first()
        self.content_object = self.content_type_object.model_class().objects.create(
            title=self.fake.text().split('.')[0])
        self.comment = Comment.objects.create(
            author=self.user,
            object_id=self.content_object.id,
            content_type=self.content_type_object,
            text=self.fake.text().split('.')[0],

        )
        self.file_list = []
        for o in range(3):
            self.file_list.append(self.comment.files.create(
                file_source=SimpleUploadedFile("file.txt", b"file_content"),
                author=self.user
            ))

    def tearDown(self):
        self.user.delete()
        self.user_two.delete()
        shutil.rmtree(os.path.join(settings.BASE_DIR, 'tests'), ignore_errors=True)

    def test_file_list(self):
        response = self.client.get(reverse('comments:comment-file-list', args=[self.comment.id]))
        self.assertEqual(response.status_code, status.HTTP_200_OK, msg=response.json())
        self.assertEqual(len(response.json()), 3)

    def test_file_create(self):
        data = {
            'file_source': SimpleUploadedFile("file.txt", b"file_content")
        }
        response = self.client.post(reverse('comments:comment-file-list', args=[self.comment.id]), data,
                                    format='multipart')
        self.assertEqual(response.status_code, status.HTTP_403_FORBIDDEN, msg=response.json())

        data = {
            'file_source': SimpleUploadedFile("file.txt", b"file_content")
        }
        self.client.force_login(self.user_two)
        response = self.client.post(reverse('comments:comment-file-list', args=[self.comment.id]), data,
                                    format='multipart')
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST, msg=response.json())

        data = {
            'file_source': SimpleUploadedFile("file.txt", b"file_content")
        }
        self.client.force_login(self.user)
        response = self.client.post(reverse('comments:comment-file-list', args=[self.comment.id]), data,
                                    format='multipart')
        self.assertEqual(response.status_code, status.HTTP_201_CREATED, msg=response.json())


class FileDetailDeleteTest(APITestCase):
    def setUp(self):
        self.fake = Faker()
        self.user = User.objects.create_user('test_user')
        self.user_two = User.objects.create_user('test_user_two')
        self.content_type_object = ContentType.objects.filter(CONTENT_TYPE_CHOICES).first()
        self.content_object = self.content_type_object.model_class().objects.create(
            title=self.fake.text().split('.')[0])
        self.comment = Comment.objects.create(
            author=self.user,
            object_id=self.content_object.id,
            content_type=self.content_type_object,
            text=self.fake.text().split('.')[0],

        )
        self.file = self.comment.files.create(
            file_source=SimpleUploadedFile("file.txt", b"file_content"),
            author=self.user
        )

    def tearDown(self):
        self.user.delete()
        self.user_two.delete()
        shutil.rmtree(os.path.join(settings.BASE_DIR, 'tests'), ignore_errors=True)

    def test_file_detail(self):
        response = self.client.get(reverse('comments:comment-file-detail', args=[self.comment.id, self.file.id]))
        self.assertEqual(response.status_code, status.HTTP_200_OK, msg=response.json())

    def test_file_delete(self):
        response = self.client.delete(reverse('comments:comment-file-detail', args=[self.comment.id, self.file.id]))
        self.assertEqual(response.status_code, status.HTTP_403_FORBIDDEN, msg=response.json())

        self.client.force_login(self.user_two)
        response = self.client.delete(reverse('comments:comment-file-detail', args=[self.comment.id, self.file.id]))
        self.assertEqual(response.status_code, status.HTTP_403_FORBIDDEN, msg=response.json())

        self.client.force_login(self.user)
        response = self.client.delete(reverse('comments:comment-file-detail', args=[self.comment.id, self.file.id]))
        self.assertEqual(response.status_code, status.HTTP_204_NO_CONTENT)


class SubscribeUser(APITestCase):
    def setUp(self):
        self.fake = Faker()
        self.user = User.objects.create_user('test_user')
        self.user_two = User.objects.create_user('test_user_two')
        self.content_type_object = ContentType.objects.filter(CONTENT_TYPE_CHOICES).first()
        self.content_object = self.content_type_object.model_class().objects.create(
            title=self.fake.text().split('.')[0])
        self.url_args = [self.content_type_object.id, self.content_object.id]

    def tearDown(self):
        self.user.delete()
        self.user_two.delete()

    def test_subscribe_to_detail(self):
        response = self.client.get(reverse('comments:subscribe-to-detail', args=self.url_args))
        self.assertEqual(response.status_code, status.HTTP_403_FORBIDDEN, msg=response.json())

        response = self.client.post(reverse('comments:subscribe-to-detail', args=self.url_args))
        self.assertEqual(response.status_code, status.HTTP_403_FORBIDDEN, msg=response.json())

        self.client.force_login(self.user)
        response = self.client.get(reverse('comments:subscribe-to-detail', args=self.url_args))
        self.assertEqual(response.status_code, status.HTTP_404_NOT_FOUND, msg=response.json())

        self.client.force_login(self.user_two)
        response = self.client.delete(reverse('comments:subscribe-to-detail', args=self.url_args))
        self.assertEqual(response.status_code, status.HTTP_404_NOT_FOUND, msg=response.json())

        response = self.client.post(reverse('comments:subscribe-to-detail', args=self.url_args))
        self.assertEqual(response.status_code, status.HTTP_201_CREATED, msg=response.json())

        self.client.force_login(self.user)
        response = self.client.delete(reverse('comments:subscribe-to-detail', args=self.url_args))
        self.assertEqual(response.status_code, status.HTTP_404_NOT_FOUND, msg=response.json())

        response = self.client.post(reverse('comments:subscribe-to-detail', args=self.url_args))
        self.assertEqual(response.status_code, status.HTTP_201_CREATED, msg=response.json())

        response = self.client.delete(reverse('comments:subscribe-to-detail', args=self.url_args))
        self.assertEqual(response.status_code, status.HTTP_204_NO_CONTENT)
