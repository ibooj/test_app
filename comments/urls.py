from django.urls import path, include
from fcm_django.api.rest_framework import FCMDeviceAuthorizedViewSet
from rest_framework.routers import DefaultRouter

from .views import (CommentListCreate, CommentDetailEditDelete, CommentListByUser, FileListCreate, FileDetailDelete,
                    SubscribeUser)

fcm_django_router = DefaultRouter()
fcm_django_router.register(r'devices', FCMDeviceAuthorizedViewSet)

app_name = 'comments'

urlpatterns = [
    path('', include(fcm_django_router.urls)),
    path('<int:content_type_id>/<int:object_id>/', CommentListCreate.as_view(), name='comment-list'),
    path('<int:content_type_id>/<int:object_id>/<int:parent_id>/', CommentListCreate.as_view(),
         name='answer-comment-list'),
    path('by-user/<int:author_id>/', CommentListByUser.as_view(), name='user-comment-list'),
    path('edit/<int:pk>/', CommentDetailEditDelete.as_view(), name='comment-detail'),
    path('edit/<int:comment_id>/file/', FileListCreate.as_view(), name='comment-file-list'),
    path('edit/<int:comment_id>/file/<int:pk>/', FileDetailDelete.as_view(), name='comment-file-detail'),
    path('<int:content_type_id>/<int:object_id>/subscribe_to/', SubscribeUser.as_view(),
         name='subscribe-to-detail'),
]
