import reversion
from django.shortcuts import get_object_or_404
from django_filters import rest_framework as filters
from rest_condition import Or, And
from rest_framework import generics
from rest_framework.exceptions import ValidationError
from rest_framework.permissions import IsAuthenticated

from .models import Comment, CommentFile, SubscribedUser
from .permissions import IsPOSTRequest, IsPUTRequest, IsDELETERequest, IsOwner, IsAuthenticatedOrReadOnly, IsGETRequest
from .serializers import CommentListSerializer, CommentFileSerializer, SubscribedUserSerializer


class CommentsFilter(filters.FilterSet):
    created__gte = filters.DateTimeFilter(name="created", lookup_expr="gte")
    created__lte = filters.DateTimeFilter(name="created", lookup_expr="lte")
    level = filters.NumberFilter(name="level")

    class Meta:
        model = Comment
        fields = ['created__gte', 'created__lte', 'level']


class CommentListCreate(generics.ListCreateAPIView):
    serializer_class = CommentListSerializer
    queryset = Comment.objects
    permission_classes = [Or(
        And(IsAuthenticated, IsPOSTRequest),
        IsAuthenticatedOrReadOnly
    )]
    filter_class = CommentsFilter

    def get_queryset(self):
        if self.kwargs.get('parent_id'):
            kwargs = self.kwargs.copy()
            kwargs['id'] = kwargs['parent_id']
            del kwargs['parent_id']
            queryset = self.queryset.get(**kwargs).get_descendants()
        else:
            queryset = super().get_queryset().select_related('author').prefetch_related('files')
            queryset = queryset.filter(**self.kwargs)

        return queryset

    def list(self, request, *args, **kwargs):
        if self.request.query_params.get('format') == 'xml':
            self._paginator = None
        return super().list(request, *args, **kwargs)

    def perform_create(self, serializer):
        serializer.save(author=self.request.user, **self.kwargs)


class CommentDetailEditDelete(generics.RetrieveUpdateDestroyAPIView):
    serializer_class = CommentListSerializer
    queryset = Comment.objects.all()
    permission_classes = [Or(
        And(IsOwner, Or(IsPUTRequest, IsDELETERequest)),
        IsAuthenticatedOrReadOnly
    )]

    def perform_destroy(self, instance):
        if instance.get_descendant_count():
            raise ValidationError("You can't delete this comment it has child object.")
        instance.delete()

    def perform_update(self, serializer):
        message = 'Changed text.'
        if serializer.validated_data['text'] == serializer.instance.text:
            message = 'No fields changed.'
        reversion.set_comment(message)
        serializer.save()


class CommentListByUser(generics.ListAPIView):
    serializer_class = CommentListSerializer
    queryset = Comment.objects.all()
    filter_class = CommentsFilter

    def list(self, request, *args, **kwargs):
        if self.request.query_params.get('format') == 'xml':
            self._paginator = None
        return super().list(request, *args, **kwargs)

    def get_queryset(self):
        queryset = super().get_queryset().select_related('author')
        return queryset.filter(**self.kwargs)


class FileListCreate(generics.ListCreateAPIView):
    queryset = CommentFile.objects.all()
    pagination_class = None
    serializer_class = CommentFileSerializer
    permission_classes = [Or(
        And(IsAuthenticated, IsPOSTRequest),
        IsAuthenticatedOrReadOnly
    )]

    def get_queryset(self):
        queryset = super().get_queryset()
        return queryset.filter(**self.kwargs)

    def perform_create(self, serializer):
        serializer.save(author=self.request.user, **self.kwargs)


class FileDetailDelete(generics.RetrieveDestroyAPIView):
    queryset = CommentFile.objects
    serializer_class = CommentFileSerializer
    permission_classes = [Or(
        And(IsOwner, IsDELETERequest),
        IsAuthenticatedOrReadOnly
    )]


class SubscribeUser(generics.CreateAPIView, generics.RetrieveDestroyAPIView):
    queryset = SubscribedUser.objects
    serializer_class = SubscribedUserSerializer
    permission_classes = [And(
        IsAuthenticated,
        Or(IsGETRequest, IsPOSTRequest, IsDELETERequest),
    )]
    pagination_class = None

    def get_queryset(self):
        queryset = super().get_queryset()
        return queryset.filter(author=self.request.user, **self.kwargs)

    def perform_create(self, serializer):
        if self.get_queryset().exists():
            raise ValidationError("You are already subscribed.")
        serializer.save(author=self.request.user, **self.kwargs)

    def get_object(self):
        return get_object_or_404(self.get_queryset())

